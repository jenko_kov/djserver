#  coding=UTF-8
__author__ = 'jenko_2'
from oper_server.hdd.models import DicGames, DicGamesComp, DicGamesOffice
from oper_server.hdd import consts
from oper_server.models import Club


class HDDHelper():

    def __init__(self, comp_obj, data_map):
        self.compObj = comp_obj
        self.club = comp_obj.club
        self.data_list = data_map
        self.required_games = DicGames.objects.filter(dicgamesoffice__club_id=self.club.club_id)
        self.games_to_record = list()

    def checkAllGames(self):
        res = [dict(folder=game_dict['folder'],
                    status=self.__get_game_status(game_dict),
                    steam_id=game_dict['steam_id']) for game_dict in self.data_list]
        self.__update_games()
        res.extend(self.__append_games(res))  # додаємо ігри, яких немає в data_map
        return res

    def __update_games(self):
        DicGamesComp.objects.filter(comp=self.compObj).delete()
        list_to_insert = list()
        for game_dict in self.games_to_record:
            list_to_insert.append(DicGamesComp(comp=self.compObj,
                                               dic_games=game_dict['game'],
                                               size=game_dict['size']))
        DicGamesComp.objects.bulk_create(list_to_insert)

    def __append_games(self, res):
        append_list = list()
        for game in self.required_games:
            if game.steam_id:
                if not filter(lambda x: x['steam_id'] == unicode(game.steam_id) and x['folder'] == game.folder_name, res):
                    append_list.append(dict(folder=game.folder_name,
                                            status=consts.GAME_STATUS__MUST_BE_ADDED,
                                            steam_id=game.steam_id))
            else:
                if not filter(lambda x: x['folder'] == game.folder_name, res):
                    append_list.append(dict(folder=game.folder_name,
                                            status=consts.GAME_STATUS__MUST_BE_ADDED,
                                            steam_id=None))
        return append_list

    def __get_game_status(self, game_dict):
        if game_dict['steam_id']:
            game_obj = self.required_games.filter(steam_id=game_dict['steam_id'], folder_name=game_dict['folder'])
        else:
            game_obj = self.required_games.filter(folder_name=game_dict['folder'])
        if len(game_obj) == 1:
            res = consts.GAME_STATUS__WRONG_SIZE if abs(int(game_dict['size']) - game_obj[0].size) > game_obj[0].accuracy else consts.GAME_STATUS__GOOD
            self.games_to_record.append(dict(game=game_obj[0], size=game_dict['size']))
        elif not game_obj:
            res = consts.GAME_STATUS__MUST_BE_DELETED
        else:
            raise RuntimeError("more than one folders with this name in db")

        return res


class IdealsProcessor():
    def __init__(self, req):
        self.req = req
        self.oper_type = req['type']

    def process(self):
        functions_map = {'ins': self.__ins, 'del': self.__del, 'mod': self.__mod, 'list': self.__list}
        func = functions_map.get(self.oper_type)
        return func()

    @staticmethod
    def __add_game_to_clubs(game_obj, clubs_map):
        curr_clubs = [rec.club for rec in DicGamesOffice.objects.filter(dic_games=game_obj)]
        req_clubs = Club.objects.filter(club_id__in=clubs_map)
        for club in curr_clubs:
            if club not in req_clubs:
                DicGamesOffice.objects.filter(dic_games=game_obj, club=club).delete()
        for club in req_clubs:
            if club not in curr_clubs:
                DicGamesOffice(dic_games=game_obj, club=club).save()

    def __list(self):
        res = list()
        games_list = DicGames.objects.all()
        for game in games_list:
            clubs = [club.club_id for club in DicGamesOffice.objects.filter(dic_games=game)]
            res.append(dict(folder=game.folder_name,
                            size=game.size,
                            accuracy=game.accuracy,
                            clubs_map=clubs,
                            steam_id=game.steam_id))
        return res

    def __ins(self):
        if not DicGames.objects.filter(folder_name=self.req[consts.FOLDER_FIELD], steam_id=self.req[consts.STEAM_ID_FIELD]):
            new_game = DicGames(folder_name=self.req[consts.FOLDER_FIELD],
                                size=self.req[consts.SIZE_FIELD],
                                accuracy=self.req[consts.ACCURACY_FIELD])
            if consts.STEAM_ID_FIELD in self.req and self.req[consts.STEAM_ID_FIELD]:
                new_game.steam_id = int(self.req[consts.STEAM_ID_FIELD])
            new_game.save()
            self.__add_game_to_clubs(new_game, self.req[consts.CLUBS_MAP_FIELD])
            res = {'dic_games_id': new_game.dic_games_id}
        else:
            res = {'error': 'Folder with this name already exist'}
        return res

    def __mod(self):
        try:
            game = DicGames.objects.get(folder_name=self.req[consts.FOLDER_FIELD], steam_id=self.req[consts.STEAM_ID_FIELD])
            game.size = self.req[consts.SIZE_FIELD]
            game.accuracy = self.req[consts.ACCURACY_FIELD]
            if consts.STEAM_ID_FIELD in self.req and self.req[consts.STEAM_ID_FIELD]:
                game.steam_id = int(self.req[consts.STEAM_ID_FIELD])
            else:
                game.steam_id = None
            self.__add_game_to_clubs(game, self.req[consts.CLUBS_MAP_FIELD])
            game.save()
            res = 0
        except:
            res = {'error': 'Folder does not exist'}
        return res

    def __del(self):
        try:
            DicGames.objects.get(folder_name=self.req[consts.FOLDER_FIELD], steam_id=self.req[consts.STEAM_ID_FIELD]).delete()
            res = 0
        except:
            res = {'error': 'Folder does not exist'}
        return res

    def __none_type(self):
        return {'error': 'No such operation type'}