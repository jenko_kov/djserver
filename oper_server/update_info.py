import time
from models import Club, Comp, Torrent, Avp, HardInfo
from oper_server.hdd.helper import HDDHelper


def get_time():
    t = time.localtime()
    return '{0}-{1}-{2} {3}:{4}:{5}'.format(t[0], str(t[1]).zfill(2), str(t[2]).zfill(2), str(t[3]).zfill(2), str(t[4]).zfill(2), str(t[5]).zfill(2))


def update_data(req):
    club = Club.objects.filter(club_id=req["club"])
    if "smart_status" in req:
        smart = req["smart_status"]
    else:
        smart = None
    res = False
    if club:
        clubObj = club[0]
        try:
            comp = Comp.objects.get(club_id=clubObj, num=req["comp"])
        except:
            comp = None
        if comp:
            compObj = comp
            compObj.freeC = int(req["freeC"])
            compObj.freeD = int(req["freeD"])
            compObj.mod_time = get_time()
            compObj.smart_status = smart
            compObj.save()
            res = {'status': 'updated'}
        # else:
        #     comp = Comp()
        #     comp.club = clubObj
        #     comp.num = req["comp"]
        #     comp.freeC = int(req["freeC"])
        #     comp.freeD = int(req["freeD"])
        #     comp.mod_time = get_time()
        #     comp.smart_status = smart
        #     comp.save()
        #     res = {'status': 'added'}
    else:
        res = {'status': 'club not defined'}

    return res


def update_tor(req):
    workstation = Comp.objects.get(num=req['comp'], club=req['club'])
    comp_id = workstation.id
    try:
        qry = Torrent.objects.get(comp=comp_id, torrent_name=req['torrent_name'])
    except:
        qry = Torrent()

    if not qry:
        qry = Torrent()
    qry.comp = workstation
    qry.ins_time = req['ins_time']
    qry.status = req['status']
    qry.torrent_name = req['torrent_name']
    qry.save()

    return {'torrent_id': qry.pk}


def update_avp(req):
    try:
        workstation = Comp.objects.get(num=req['comp'], club=req['club'])
        comp_id = workstation.id
        rec = Avp.objects.get(comp=comp_id)
        if 'start' in req:
            rec.start = req['start']
            rec.status = 0
        if 'end' in req:
            rec.end = req['end']
            rec.status = 1
        rec.save()
        res = {'updated': 'Ok'}
    except:
        res = {'error': 'Comp not found'}
    return res


def update_hard(req):
    try:
        workstation = Comp.objects.get(num=req['comp'], club=req['club'])
        comp_id = workstation.id
        rec = HardInfo.objects.filter(comp=comp_id)
        if len(rec) == 0:
            obj = HardInfo(comp_id=comp_id)
        else:
            obj = rec[0]
        req.pop('comp', None)
        req.pop('club', None)
        for param, value in req.iteritems():
            try:
                setattr(obj, param, value)
            except:
                pass
        obj.save()
        res = {'updated': 'Ok'}
    except:
        res = {'error': 'Error while updating hardinfo'}
    return res


def update_hdd(req):
    try:
        workstation = Comp.objects.get(num=req['comp'], club=req['club'])
        if workstation:
            proc = HDDHelper(workstation, req['folder_list'])
            res = proc.checkAllGames()
        else:
            res = {'error': 'Workstation not found'}
    except Exception as e:
        res = {'error': str(e)}
    return res